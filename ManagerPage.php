<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Manager</title>
    <link rel="stylesheet" type="text/css" href="myStyle.css">
    <script>
        function openPage(event, page) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            document.getElementById(page).style.display = "block";
            event.currentTarget.className += " active";
        }
    </script>
    <script>
        var delete_cookie = function (name) {
            document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        };
    </script>
    <script>
        function timeout() {
            setTimeout(function () {
                delete_cookie('ID');
                alert("Time out!");
                window.location.href = 'index.php';
            }, 2 * 60000);

        }
    </script>
    <script language="JavaScript">
        function AddStock() {
            var x = document.getElementById('stockName').value;
            if (x === '') {
                alert('Please input Stock Name!');
            } else {
                document.getElementById('addstockfrom').submit();
            }

        }
    </script>
    <script>
    </script>
    <script>
        function Nullselect() {
            alert('Please select one!!');
            window.location.href = 'ManagerPage.php';
        }
    </script>
    <script>
        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            var d = today.getDate();
            var mon = today.getMonth();
            var y = today.getFullYear();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML = "Date/Time : " + y + "/" + (mon + 1) + "/" + d + " / ";
            if (h < 11) {
                document.getElementById('txt').innerHTML = "Date/Time : " + y + "/" + (mon + 1) + "/" + d + " /    AM " + h + ":" + m + ":" + s;
            } else {
                document.getElementById('txt').innerHTML = "Date/Time : " + y + "/" + (mon + 1) + "/" + d + " /    PM " + h + ":" + m + ":" + s;
            }
            var t = setTimeout(startTime, 500);
        }

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i
            };
            return i;
        }
    </script>
</head>
<body id="background" onload="startTime()">
<?php
if (!isset($_COOKIE['ID'])) {
    echo "<script type=\"text/javascript\">
         alert('You are not logged in yet!');
         window.location.href='index.php';
    </script>";

} else {
    $hostname = "127.0.0.1";
    $dbname = "projectDB";
    $username = "root";
    $password = "";
    $conn = mysqli_connect($hostname, $username, $password, $dbname)
    or die (mysqli_connect_error());
    $sql = "SELECT * FROM `managers` WHERE ManagerId ={$_COOKIE['ID']}";
    $rs = mysqli_query($conn, $sql);
    while ($rc = mysqli_fetch_assoc($rs)) {
        echo "<p align='right'>
        Welcome, {$rc['Name']}";
        echo "
    <input type='submit' id='logOut' value='Logout' onclick=\"window.location.href='Logout.php'\" >
          </p>";
        echo "<script type='text/javascript'>
            timeout();
        </script>";
    }
    if (isset($_GET['nullSelect'])) {
        echo "<script type='text/javascript'>
            Nullselect();
        </script>";
    }
}

?>
<br>
<div id="txt" ALIGN="LEFT"></div>
<h1 id="Title">IVE Restaurant Group Limited</h1>
<div class="tab">
    <button class="tablinks" onclick="openPage(event, 'PendingList')" id="btnPendingList">Pending Order List</button>
    <button class="tablinks" onclick="openPage(event, 'List')" id="btnList">Order List</button>
    <button class="tablinks" onclick="openPage(event, 'stock')" id="btnstock">Stock</button>
</div>
<div id="PendingList" class="tabcontent">
    <p align="left"><input type="submit" value="Approve" name="approve" id="btn" form="ActionForm">
        <input type="submit" value="Delete" name="ManDelete" id="btn" form="ActionForm"></p>
    <?php
    $sql = "SELECT `orders`.`OrderId`, `suppliers`.`Name` as supName, `stock`.`Name` as stockName, `orders`.`Amount`, `orders`.`PurchaseDate`, `orders`.`DeliveryDate`, `orders`.`ReceivedDate` FROM `orders`
    LEFT JOIN `supplierstock` ON `orders`.`SupplierStockId` = `supplierstock`.`SupplierStockId`
    LEFT JOIN `stock` ON `supplierstock`.`StockId` = `stock`.`StockId`
    LEFT JOIN `suppliers` ON `supplierstock`.`SupplierId` = `suppliers`.`SupplierId`
    WHERE (`orders`.`OrderId` is not null) AND `orders`.`Approved`= 0";
    $rs = mysqli_query($conn, $sql);
    ?>
    <div id="orderListOverFlow">
        <form method="post" action="PendingOrderListAction.php" id="ActionForm">
            <table border="1" id="orderTable" align="center">
                <thead>
                <tr align="center">
                    <th></th>
                    <th>Order ID</th>
                    <th>Supplier Name</th>
                    <th>Stock Name</th>
                    <th>Order Amount</th>
                    <th>Order Purchase Date</th>
                    <th>Order Delivery Date</th>
                    <th>Order Received Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($rc = mysqli_fetch_assoc($rs)) {
                    echo "<tr>
                <td><input type='checkbox' name='checkbox[]' class='check' id='checkbox' value='{$rc['OrderId']}' onclick='checkTrue();'></td>
                <td>{$rc['OrderId']}</td>
                <td>{$rc['supName']}</td>
                <td>{$rc['stockName']}</td>
                <td>{$rc['Amount']}</td>
                <td>{$rc['PurchaseDate']}</td>
                <td>{$rc['DeliveryDate']}</td>
                <td>{$rc['ReceivedDate']}</td>
            </tr>";
                }
                ?>
                </tbody>
            </table>
            <style>
                .check {
                    background-color: red;
                    color: #fff;
                    font-weight: bold
                }
            </style>
            <script type="text/javascript">
                function checkTrue() {
                    var check = document.getElementsByClassName('check');
                    for (var i = 0; i < check.length; i++) {
                        if (check[i].checked) {

                        } else {
                            check[i].onclick = function () {
                                this.parentElement.classList.toggle("check");
                            };
                        }
                    }
                }

                checkTrue();
            </script>
        </form>
    </div>
</div>
<div id="List" class="tabcontent">
    <?php
    $sql = "SELECT `orders`.`OrderId`, `suppliers`.`Name` as supName, `stock`.`Name` as stockName, `orders`.`Amount`, `orders`.`PurchaseDate`, `orders`.`DeliveryDate`, `orders`.`ReceivedDate` FROM `orders`
    LEFT JOIN `supplierstock` ON `orders`.`SupplierStockId` = `supplierstock`.`SupplierStockId`
    LEFT JOIN `stock` ON `supplierstock`.`StockId` = `stock`.`StockId`
    LEFT JOIN `suppliers` ON `supplierstock`.`SupplierId` = `suppliers`.`SupplierId`
    WHERE (`orders`.`OrderId` is not null) AND `orders`.`Approved`= 1";
    $rs = mysqli_query($conn, $sql);
    ?>
    <div id="orderListOverFlow">
        <form method="post" action="PendingOrderListAction.php">
            <table border="1" id="orderTable" align="center">
                <thead>
                <tr align="center">
                    <th>Order ID</th>
                    <th>Supplier Name</th>
                    <th>Stock Name</th>
                    <th>Order Amount</th>
                    <th>Order Purchase Date</th>
                    <th>Order Delivery Date</th>
                    <th>Order Received Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($rc = mysqli_fetch_assoc($rs)) {
                    echo "<tr>
                <td>{$rc['OrderId']}</td>
                <td>{$rc['supName']}</td>
                <td>{$rc['stockName']}</td>
                <td>{$rc['Amount']}</td>
                <td>{$rc['PurchaseDate']}</td>
                <td>{$rc['DeliveryDate']}</td>
                <td>{$rc['ReceivedDate']}</td>
            </tr>";
                }
                ?>
                </tbody>
            </table>
            <br>
    </div>
    </form>
</div>
<div id="stock" class="tabcontent">
    <div id="orderListOverFlow">
        <table id="listtable" align="center" border="1">
            <thead>
            <tr>
                <th>Stock Id</th>
                <th>Manager Id</th>
                <th>Stock name</th>
            <tr>
            </thead>
            <tbody>
            <?php
            $conn = mysqli_connect("127.0.0.1", "root", "", "projectdb")
            or die (mysqli_connect_error());
            $sql = "SELECT * FROM stock";
            $rs = mysqli_query($conn, $sql) or die(mysqli_error($conn));
            while ($rc = mysqli_fetch_assoc($rs)) {
                echo "<tr>
                    <td align='center'>{$rc['StockId']}</td>
                    <td align='center'>{$rc['ManagerId']}</td>
                    <td align='center'>{$rc['Name']}</td>
                </tr>";
            }
            ?>
            </tbody>
        </table>
    </div>
    <br>
    <div>
        <?php
        if (isset($_GET["add"])) {
            if (isset($_GET["SuccMsg"])) {
                echo "<script>
                window.location.href='ManagerPage.php?add=1';
                </script>";
            } else {
                $ID = $_COOKIE['ID'];
                echo "
            <form method='post' id='addstockfrom' action='AddStock.php'>
                    Manager ID  : <input type='text' value='$ID' id='mid' name='mid' readonly><br>
                    <br>
                    Stock Name : <input type='text' id='stockName' name='stockName'><br>
                    <br>
                    <input type='button'id='btn' value='Add' onclick='AddStock()'>
            </form>
            <script>
                document.getElementById('btnstock').click(); //Open stock page
            </script>";
            }
        } else {
            echo "<script>
             document.getElementById('btnPendingList').click(); //default Open
        </script>
        <input type='submit' onclick=\"window.location.href='ManagerPage.php?add=true'\" value='Add Stock' id='addStockBtn'>";
        }
        ?>
    </div>

</body>
</html>