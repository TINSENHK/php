<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="myStyle.css">
    <script>
        function Wronginput() {
            alert("Wrong ID or Password!");
            window.location.href="index.php";
        }
    </script>
</head>
<body id="background">
<?php
    if (isset($_GET['Msg'])) {
        echo "<script>Wronginput();</script>";
    }
    ?>
<h1 id="Title">IVE Restaurant Group Limited</h1>
<br><br><br><br><br><br><br>
<form action="login.php" method="post">
    <h2 id="sencondTitle">Food System</h2>
    <table id="loginTable" align="center" border="1">
        <tr>
            <td><label>Staff ID : </label></td>
            <td><input type="text" name="ID" value="" class="checkboxIDPW" required></td>
        </tr>
        <td><label>Password : </label></td>
        <td><input type="password" name="pw" value="" class="checkboxIDPW" REQUIRED></td>
        <tr>
            <td><label>Position :</label></td>
            <td><select name="position" id="selection">
                <option value="Restaurant">Restaurant</option>
                <option value="Manager">Manager</option>
            </select>
            </td>
        </tr>
    </table>
    <p align="center">
        <input type="submit" name="login_btn" value="Login" id="loginButton">
    </p>
</form>
</body>
</html>