<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Restaurant</title>
    <link rel="stylesheet" type="text/css" href="myStyle.css">
    <script>
        function timeout() {
            setTimeout(function () {
                delete_cookie('ID');
                alert("Time out!");
                window.location.href = 'index.php';
            }, 2 * 60000);

        }
    </script>
    <script>
        function openPage(evt, page) {
            var i, tabcontent, tablinks;

            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            document.getElementById(page).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
    <script>
        function Nullselect() {
            alert('Please select one!!');
            window.location.href = 'RestaurantPage.php';
        }
    </script>
    <script>
        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            var d = today.getDate();
            var mon = today.getMonth();
            var y = today.getFullYear();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('txt').innerHTML = "Date/Time : " + y + "/" + (mon + 1) + "/" + d + "/";
            if (h < 11) {
                document.getElementById('txt').innerHTML = "Date/Time : " + y + "/" + (mon + 1) + "/" + d + " /    AM " + h + ":" + m + ":" + s;
            } else {
                document.getElementById('txt').innerHTML = "Date/Time : " + y + "/" + (mon + 1) + "/" + d + " /    PM " + h + ":" + m + ":" + s;
            }
            var t = setTimeout(startTime, 500);
        }

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i
            };
            return i;
        }
    </script>
</head>
<body id="background" onload="startTime()">
<?php
if (!isset($_COOKIE['ID'])) {
    echo "<script type=\"text/javascript\">
         alert('You are not logged in yet!');
         window.location.href='index.php';
    </script>";
} else {
    $hostname = "127.0.0.1";
    $dbname = "projectDB";
    $username = "root";
    $password = "";
    $conn = mysqli_connect($hostname, $username, $password, $dbname)
    or die (mysqli_connect_error());
    $sql = "SELECT * FROM `restaurants` WHERE RestaurantId ={$_COOKIE['ID']}";
    $rs = mysqli_query($conn, $sql);
    while ($rc = mysqli_fetch_assoc($rs)) {
        echo "<p align='right'> 
            <input type='submit' id='Edit' value='Edit' onclick=\"window.location.href='RestaurantPage.php?EditInfo=true'\")'>
    <input type='submit' id='logOut' value='Logout' onclick=\"window.location.href='Logout.php'\" >
          </p>";
        echo "<script type=\"text/javascript\">
            timeout();
        </script>";
    }
}
?>
<div id="txt" ALIGN="LEFT"></div>
<h1 id="Title">IVE Restaurant Group Limited</h1>
<?php
$hostname = "127.0.0.1";
$username = "root";
$password = "";
$dbname = "projectDB";
$conn = @mysqli_connect($hostname, $username, $password, $dbname)
or die ("<p id='FailMsg'>Server Connection Failed</p><p id='FailMsg'>Please try again!</p>
<p id='FailMsg'><a href='index.php' >Refresh</a></p>");
$result = mysqli_query($conn, "SELECT `Name` FROM `restaurants` WHERE `RestaurantId` = {$_COOKIE['ID']}") or die ();

while ($rc = mysqli_fetch_assoc($result)) {
    echo "<p align='center'>{$rc['Name']}</p>";
}

$result = mysqli_query($conn, "SELECT `Descriptions` FROM `restaurants` WHERE `RestaurantId` = {$_COOKIE['ID']}") or die ();
while ($rc = mysqli_fetch_assoc($result)) {
    echo "<p align='center'>{$rc['Descriptions']}</p>";
}
if (isset($_GET['nullSelect'])) {
    echo "<script type='text/javascript'>
            Nullselect();
        </script>";
}
?>
<div class="tab">
    <button class="tablinks" onclick="openPage(event, 'List')" id="tabList">Order List</button>
    <button class="tablinks" onclick="openPage(event, 'New')">Create Order</button>
    <button class="tablinks" onclick="openPage(event, 'stock')">Stock</button>
</div>
<div id="List" class="tabcontent">
    <?php
    $sql = "SELECT `orders`.`OrderId`, `suppliers`.`Name` as supName, `stock`.`Name` as stockName, `orders`.`Amount`, `orders`.`PurchaseDate`, `orders`.`DeliveryDate`, `orders`.`ReceivedDate` FROM `orders`
    LEFT JOIN `supplierstock` ON `orders`.`SupplierStockId` = `supplierstock`.`SupplierStockId`
    LEFT JOIN `stock` ON `supplierstock`.`StockId` = `stock`.`StockId`
    LEFT JOIN `suppliers` ON `supplierstock`.`SupplierId` = `suppliers`.`SupplierId`
    WHERE (`orders`.`OrderId` is not null) AND `orders`.`Approved`= 0 AND RestaurantId = {$_COOKIE['ID']}"; /////add rest id
    $rs = mysqli_query($conn, $sql);
    ?>
    <p align="left">
        <input type="submit" name="RestDelete" value="Delete" id="btn" form="deleteorder">
    </p>
    <div id="orderListOverFlow">
        <form method="post" action="PendingOrderListAction.php" id="deleteorder">
            <table border="2" id="orderTable" align="center">
                <thead>
                <tr align="center">
                    <th></th>
                    <th>Order ID</th>
                    <th>Supplier Name</th>
                    <th>Stock Name</th>
                    <th>Order Amount</th>
                    <th>Order Purchase Date</th>
                    <th>Order Delivery Date</th>
                    <th>Order Received Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($rc = mysqli_fetch_assoc($rs)) {
                    echo "<tr class='abc'>
                <td><input type='checkbox' name='checkbox[]' class='check' id='checkbox' value='{$rc['OrderId']}' onclick='checkTrue();'></td>
                <td>{$rc['OrderId']}</td>
                <td>{$rc['supName']}</td>
                <td>{$rc['stockName']}</td>
                <td>{$rc['Amount']}</td>
                <td>{$rc['PurchaseDate']}</td>
                <td>{$rc['DeliveryDate']}</td>
                <td>{$rc['ReceivedDate']}</td>
            </tr>";
                }
                ?>
                </tbody>
            </table>
            <style>
                .check {
                    background-color: red;
                    color: #fff;
                    font-weight: bold
                }
            </style>
            <script type="text/javascript">
                function checkTrue() {
                    var check = document.getElementsByClassName('check');
                    for (var i = 0; i < check.length; i++) {
                        if (check[i].checked) {

                        } else {
                            check[i].onclick = function () {
                                this.parentElement.classList.toggle("check");
                            };
                        }
                    }
                }

                checkTrue();
            </script>
            <br>
        </form>
    </div>
</div>
<div id="New" class="tabcontent">
    <?php
    $sql = "SELECT supplierstock.SupplierStockId, stock.Name FROM supplierstock
    LEFT JOIN stock ON supplierstock.StockId = stock.StockId WHERE (supplierstock.SupplierStockId is not null)";
    $rs = mysqli_query($conn, $sql);
    ?>
    <form name="orders" method="post" action="createOrder.php">
        <table align="center" id="createOrder">

            <tr>
                <td>Restaurant ID :</td>
                <td><input type="number" name="restaurantId" id="restaurantId" value="<?php echo "{$_COOKIE['ID']}" ?>"
                           readonly>
            </tr>
            <tr>
                <td>Supplier Stock Name (ID) :</td>
                <td><select name="supplierStockId" id="supplierStockId" style="text-align: center">
                        <?php while ($rc = mysqli_fetch_assoc($rs)):; ?>
                            <option value="<?php echo "{$rc['SupplierStockId']}"; ?>">
                                <?php echo "{$rc['Name']}( {$rc['SupplierStockId']} )" ?>
                            </option>
                        <?php endwhile; ?>
                    </select>
            </tr>
            <tr>
                <td>Amount :</td>
                <td><input type="number" name="amount" id="amount" min="1" required></td>
            </tr>
            <tr>
                <td>Purchase Date :</td>
                <td><input type="date" name="purchaseDate" id="purchaseDate"
                           value="<?php echo date("Y-m-d"); ?>" required></td>
            </tr>
        </table>
        <input type="submit" name="create" value="Create order" id="newOrder">
    </form>

</div>
<div id="stock" class="tabcontent">
    <?php
    $sql = "SELECT warehousestock.WarehouseStockId, stock.Name, warehousestock.Amount FROM warehousestock
    LEFT JOIN stock ON warehousestock.StockId = stock.StockId WHERE (warehousestock.Amount is not null)";
    $rs = mysqli_query($conn, $sql);
    ?>
    <table border="1" id="listtable" align="center">
        <thead>
        <tr>
            <th>Stock ID</th>
            <th>Stock Name</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
        <?php
        while ($rc = mysqli_fetch_assoc($rs)) {
            echo "
                    <tr>
                    <td>{$rc['WarehouseStockId']}</td>
                   <td>{$rc['Name']}</td>
                  <td>{$rc['Amount']}</td>
            </tr>";
        }
        ?>
        </tbody>
    </table>
</div>
<div id="EditInfo" class="tabcontent">
    <?php
    if (isset($_GET["EditInfo"])) {
        $result = mysqli_query($conn, "SELECT `Name` FROM `restaurants` WHERE `RestaurantId` = {$_COOKIE['ID']}") or die ();
        echo "
            <script>
            openPage(event, 'EditInfo');
            </script>
        ";
        echo "<form method='post' action='EditInfo.php' id='EditInfo'>";
        echo "<table align='center'>";
        echo "<tr>";
        echo "<td>Restaurant Name :</td>";
        while ($rc = mysqli_fetch_assoc($result)) {
            echo "<td><input type='text' name='RestName' id='RestName' value='{$rc['Name']}'></td>";
        }
        echo "</tr>";
        $result = mysqli_query($conn, "SELECT `Descriptions` FROM `restaurants` WHERE `RestaurantId` = {$_COOKIE['ID']}") or die ();
        echo "<tr>";
        echo "<td>Restaurant Descriptions :</td>";
        while ($rc = mysqli_fetch_assoc($result)) {
            echo "<td><textarea name='Descriptions' id='Descriptions' cols='50' rows='10' >{$rc['Descriptions']}</textarea></td>";
        }
        echo "<br><br><br>";

        echo "</tr>";
        echo "</table>";
        echo "<input type='submit' value='Save' id='save'>";
        echo "<input type='button' value='Cancel' onclick=\"window.location.href='RestaurantPage.php'\" id='cancel'>";

        echo "</form>";
    } else {
        echo "
            <script>
            document.getElementById('tabList').click();
            </script>
        ";
    }
    ?>
</div>
</body>
</html>