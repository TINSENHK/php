-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2018 年 07 月 17 日 19:18
-- 伺服器版本: 10.1.34-MariaDB
-- PHP 版本： 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `projectdb`
--

-- --------------------------------------------------------

--
-- 資料表結構 `managers`
--

CREATE TABLE `managers` (
  `ManagerId` int(11) UNSIGNED NOT NULL,
  `Name` varchar(255) NOT NULL DEFAULT '',
  `Password` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `managers`
--

INSERT INTO `managers` (`ManagerId`, `Name`, `Password`) VALUES
(1, 'Andy', '1'),
(2, 'Edward', '1');

-- --------------------------------------------------------

--
-- 資料表結構 `orders`
--

CREATE TABLE `orders` (
  `OrderId` int(11) UNSIGNED NOT NULL,
  `RestaurantId` int(11) UNSIGNED DEFAULT NULL,
  `SupplierStockId` int(11) UNSIGNED DEFAULT NULL,
  `ManagerId` int(11) UNSIGNED DEFAULT NULL,
  `WarehouseStaffId` int(11) UNSIGNED DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `Approved` tinyint(1) DEFAULT '0',
  `PurchaseDate` date DEFAULT NULL,
  `DeliveryDate` date DEFAULT NULL,
  `ReceivedDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `orders`
--

INSERT INTO `orders` (`OrderId`, `RestaurantId`, `SupplierStockId`, `ManagerId`, `WarehouseStaffId`, `Amount`, `Approved`, `PurchaseDate`, `DeliveryDate`, `ReceivedDate`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2018-07-11', '2018-07-19', '2018-07-20'),
(10, 1, 10, NULL, NULL, 190, 1, '2018-07-17', NULL, NULL),
(11, 1, 11, NULL, NULL, 450, 1, '2018-07-17', NULL, NULL),
(12, 1, 12, NULL, NULL, 350, 1, '2018-07-17', NULL, NULL),
(13, 1, 13, NULL, NULL, 950, 1, '2018-07-17', NULL, NULL),
(14, 1, 10, NULL, NULL, 190, 1, '2018-07-17', NULL, NULL),
(15, 1, 11, NULL, NULL, 450, 1, '2018-07-17', NULL, NULL),
(16, 1, 12, NULL, NULL, 350, 1, '2018-07-17', NULL, NULL),
(17, 1, 13, NULL, NULL, 950, 1, '2018-07-17', NULL, NULL),
(18, 1, 11, NULL, NULL, 450, 1, '2018-07-17', NULL, NULL),
(19, 1, 12, NULL, NULL, 350, 1, '2018-07-17', NULL, NULL),
(20, 1, 13, NULL, NULL, 950, 1, '2018-07-17', NULL, NULL),
(21, 1, 10, NULL, NULL, 190, 1, '2018-07-17', NULL, NULL),
(22, 1, 11, NULL, NULL, 450, 1, '2018-07-17', NULL, NULL),
(23, 1, 12, NULL, NULL, 350, 1, '2018-07-17', NULL, NULL),
(24, 1, 10, NULL, NULL, 950, 1, '2018-07-17', NULL, NULL),
(29, 1, 9, NULL, NULL, 140, 0, '2018-07-17', NULL, NULL),
(30, 1, 5, NULL, NULL, 450, 0, '2018-07-17', NULL, NULL),
(31, 1, 6, NULL, NULL, 350, 0, '2018-07-17', NULL, NULL),
(32, 1, 7, NULL, NULL, 950, 0, '2018-07-17', NULL, NULL),
(33, 1, 8, NULL, NULL, 120, 0, '2018-07-17', NULL, NULL),
(34, 1, 9, NULL, NULL, 140, 0, '2018-07-17', NULL, NULL),
(35, 1, 13, NULL, NULL, 950, 1, '2018-07-17', NULL, NULL),
(36, 1, 10, NULL, NULL, 190, 1, '2018-07-17', NULL, NULL),
(37, 1, 11, NULL, NULL, 450, 1, '2018-07-17', NULL, NULL),
(38, 1, 12, NULL, NULL, 350, 1, '2018-07-17', NULL, NULL),
(39, 1, 13, NULL, NULL, 950, 1, '2018-07-17', NULL, NULL),
(40, 1, 11, NULL, NULL, 450, 1, '2018-07-17', NULL, NULL),
(41, 1, 12, NULL, NULL, 350, 1, '2018-07-17', NULL, NULL),
(42, 1, 13, NULL, NULL, 950, 1, '2018-07-17', NULL, NULL),
(43, 1, 10, NULL, NULL, 190, 1, '2018-07-17', NULL, NULL),
(44, 1, 11, NULL, NULL, 450, 1, '2018-07-17', NULL, NULL),
(45, 1, 12, NULL, NULL, 350, 1, '2018-07-17', NULL, NULL),
(46, 1, 10, NULL, NULL, 950, 1, '2018-07-17', NULL, NULL),
(47, 1, 5, NULL, NULL, 450, 0, '2018-07-17', NULL, NULL),
(48, 1, 6, NULL, NULL, 350, 0, '2018-07-17', NULL, NULL),
(49, 1, 7, NULL, NULL, 950, 0, '2018-07-17', NULL, NULL),
(50, 1, 8, NULL, NULL, 120, 0, '2018-07-17', NULL, NULL),
(51, 1, 9, NULL, NULL, 140, 0, '2018-07-17', NULL, NULL),
(52, 1, 5, NULL, NULL, 450, 0, '2018-07-17', NULL, NULL),
(53, 1, 6, NULL, NULL, 350, 0, '2018-07-17', NULL, NULL),
(54, 1, 7, NULL, NULL, 950, 0, '2018-07-17', NULL, NULL),
(55, 1, 8, NULL, NULL, 120, 0, '2018-07-17', NULL, NULL),
(56, 1, 9, NULL, NULL, 140, 0, '2018-07-17', NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `restaurants`
--

CREATE TABLE `restaurants` (
  `RestaurantId` int(11) UNSIGNED NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Descriptions` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `restaurants`
--

INSERT INTO `restaurants` (`RestaurantId`, `Name`, `Password`, `Descriptions`) VALUES
(1, 'IVE Restaurant', '1', 'Welcome to IVE Restaurant.'),
(2, 'IVE HK Restaurant', '1', 'Welcome to IVE HK Restaurant');

-- --------------------------------------------------------

--
-- 資料表結構 `stock`
--

CREATE TABLE `stock` (
  `StockId` int(11) UNSIGNED NOT NULL,
  `ManagerId` int(11) UNSIGNED DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `stock`
--

INSERT INTO `stock` (`StockId`, `ManagerId`, `Name`) VALUES
(1, 1, 'COLA'),
(2, 1, 'APPLE'),
(3, 1, 'BANANA'),
(4, 1, 'EGG'),
(5, 1, 'NOODLE'),
(6, 1, 'CHICKEN'),
(7, 1, 'RICE'),
(8, 1, 'FISH'),
(9, 1, 'PROK'),
(10, 1, 'BEEF'),
(11, 1, 'SANDWICH'),
(12, 1, 'PEACH'),
(13, 1, 'BREAD');

-- --------------------------------------------------------

--
-- 資料表結構 `suppliers`
--

CREATE TABLE `suppliers` (
  `SupplierId` int(11) UNSIGNED NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `suppliers`
--

INSERT INTO `suppliers` (`SupplierId`, `Name`, `Password`) VALUES
(1, 'Admin', '1');

-- --------------------------------------------------------

--
-- 資料表結構 `supplierstock`
--

CREATE TABLE `supplierstock` (
  `SupplierStockId` int(11) UNSIGNED NOT NULL,
  `SupplierId` int(11) UNSIGNED DEFAULT NULL,
  `StockId` int(11) UNSIGNED DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `supplierstock`
--

INSERT INTO `supplierstock` (`SupplierStockId`, `SupplierId`, `StockId`, `Amount`) VALUES
(1, 1, 1, 10),
(2, 1, 2, 20),
(3, 1, 3, 400),
(4, 1, 4, 900),
(5, 1, 5, 50),
(6, 1, 6, 70),
(7, 1, 7, 2000),
(8, 1, 8, 369),
(9, 1, 9, 269),
(10, 1, 10, 359),
(11, 1, 11, 435),
(12, 1, 12, 4569),
(13, 1, 13, 781);

-- --------------------------------------------------------

--
-- 資料表結構 `warehousestaff`
--

CREATE TABLE `warehousestaff` (
  `WarehouseStaffId` int(11) UNSIGNED NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `warehousestaff`
--

INSERT INTO `warehousestaff` (`WarehouseStaffId`, `Name`, `Password`) VALUES
(1, 'Admin', '1');

-- --------------------------------------------------------

--
-- 資料表結構 `warehousestock`
--

CREATE TABLE `warehousestock` (
  `WarehouseStockId` int(11) UNSIGNED NOT NULL,
  `WarehouseStaffId` int(11) UNSIGNED DEFAULT NULL,
  `StockId` int(11) UNSIGNED DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `warehousestock`
--

INSERT INTO `warehousestock` (`WarehouseStockId`, `WarehouseStaffId`, `StockId`, `Amount`) VALUES
(1, 1, 1, 20),
(2, 1, 2, 250),
(3, 1, 3, 720),
(4, 1, 4, 820),
(5, 1, 5, 3020),
(6, 1, 6, 400),
(7, 1, 7, 520),
(8, 1, 8, 1278),
(9, 1, 9, 357),
(10, 1, 10, 1287),
(11, 1, 11, 3695),
(12, 1, 12, 254),
(13, 1, 13, 2554);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `managers`
--
ALTER TABLE `managers`
  ADD PRIMARY KEY (`ManagerId`);

--
-- 資料表索引 `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrderId`),
  ADD KEY `fk_Orders_RestaurantId` (`RestaurantId`),
  ADD KEY `fk_Orders_SupplierStockId` (`SupplierStockId`),
  ADD KEY `fk_Orders_ManagerId` (`ManagerId`),
  ADD KEY `fk_Orders_WarehouseStaffId` (`WarehouseStaffId`);

--
-- 資料表索引 `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`RestaurantId`);

--
-- 資料表索引 `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`StockId`),
  ADD KEY `fk_Stock_ManagerId` (`ManagerId`);

--
-- 資料表索引 `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`SupplierId`);

--
-- 資料表索引 `supplierstock`
--
ALTER TABLE `supplierstock`
  ADD PRIMARY KEY (`SupplierStockId`),
  ADD KEY `fk_SupplierStock_StockId` (`StockId`),
  ADD KEY `fk_SupplierStock_SupplierId` (`SupplierId`);

--
-- 資料表索引 `warehousestaff`
--
ALTER TABLE `warehousestaff`
  ADD PRIMARY KEY (`WarehouseStaffId`);

--
-- 資料表索引 `warehousestock`
--
ALTER TABLE `warehousestock`
  ADD PRIMARY KEY (`WarehouseStockId`),
  ADD KEY `fk_WarehouseStock_WarehouseStaffId` (`WarehouseStaffId`),
  ADD KEY `fk_WarehouseStock_StockId` (`StockId`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `managers`
--
ALTER TABLE `managers`
  MODIFY `ManagerId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用資料表 AUTO_INCREMENT `orders`
--
ALTER TABLE `orders`
  MODIFY `OrderId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- 使用資料表 AUTO_INCREMENT `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `RestaurantId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用資料表 AUTO_INCREMENT `stock`
--
ALTER TABLE `stock`
  MODIFY `StockId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- 使用資料表 AUTO_INCREMENT `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `SupplierId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用資料表 AUTO_INCREMENT `supplierstock`
--
ALTER TABLE `supplierstock`
  MODIFY `SupplierStockId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- 使用資料表 AUTO_INCREMENT `warehousestaff`
--
ALTER TABLE `warehousestaff`
  MODIFY `WarehouseStaffId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用資料表 AUTO_INCREMENT `warehousestock`
--
ALTER TABLE `warehousestock`
  MODIFY `WarehouseStockId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- 已匯出資料表的限制(Constraint)
--

--
-- 資料表的 Constraints `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_Orders_ManagerId` FOREIGN KEY (`ManagerId`) REFERENCES `managers` (`ManagerId`),
  ADD CONSTRAINT `fk_Orders_RestaurantId` FOREIGN KEY (`RestaurantId`) REFERENCES `restaurants` (`RestaurantId`),
  ADD CONSTRAINT `fk_Orders_SupplierStockId` FOREIGN KEY (`SupplierStockId`) REFERENCES `supplierstock` (`SupplierStockId`),
  ADD CONSTRAINT `fk_Orders_WarehouseStaffId` FOREIGN KEY (`WarehouseStaffId`) REFERENCES `warehousestaff` (`WarehouseStaffId`);

--
-- 資料表的 Constraints `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `fk_Stock_ManagerId` FOREIGN KEY (`ManagerId`) REFERENCES `managers` (`ManagerId`);

--
-- 資料表的 Constraints `supplierstock`
--
ALTER TABLE `supplierstock`
  ADD CONSTRAINT `fk_SupplierStock_StockId` FOREIGN KEY (`StockId`) REFERENCES `stock` (`StockId`),
  ADD CONSTRAINT `fk_SupplierStock_SupplierId` FOREIGN KEY (`SupplierId`) REFERENCES `suppliers` (`SupplierId`);

--
-- 資料表的 Constraints `warehousestock`
--
ALTER TABLE `warehousestock`
  ADD CONSTRAINT `fk_WarehouseStock_StockId` FOREIGN KEY (`StockId`) REFERENCES `stock` (`StockId`),
  ADD CONSTRAINT `fk_WarehouseStock_WarehouseStaffId` FOREIGN KEY (`WarehouseStaffId`) REFERENCES `warehousestaff` (`WarehouseStaffId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
